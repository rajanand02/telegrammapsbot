Telegram = require 'telegram-bot'
tg = new Telegram(process.env.TELEGRAM_BOT_TOKEN)

request = require('request')
api = process.env.GOOGLE_API_KEY

tg.on 'message', (msg) ->
  return unless msg
  query = msg.text.replace('@maps_bot','')
  url ='https://maps.googleapis.com/maps/api/place/textsearch/json?query='+query+'&key='+api
  options = {
      url: url 
      method: 'GET'
  }
  request options, (error, response, body) ->
    if !error and response.statusCode == 200
      res = JSON.parse(body)
      results = res.results
      if results and results.length isnt 0
        tg.sendLocation
          reply_to_message_id: msg.message_id
          chat_id: msg.chat.id
          latitude: results[0].geometry.location.lat
          longitude:results[0].geometry.location.lng
        address = if results[0].formatted_address then results[0].formatted_address else query
        tg.sendMessage
          text: address
          chat_id: msg.chat.id
      else
        tg.sendMessage
          text: "Sorry human I couldn't find. Can you be more specific"
          reply_to_message_id: msg.message_id
          chat_id: msg.chat.id
    else
        tg.sendMessage
          text: "I am rebooting right now. Please try again sometime"
          reply_to_message_id: msg.message_id
          chat_id: msg.chat.id


tg.start()
